import React from 'react';
import ReactDOM from 'react-dom';
import { MapsApp } from './MapsApp';

//configuro mapbox en la app: solo dobo piar y pegar estas dos lineas, la obtengo de la conf de inst de maxbox. 'mapbox-gl' no es reconocido por typescript, debo instalar la extención de dependencia
import mapboxgl from 'mapbox-gl'; // or "const mapboxgl = require('mapbox-gl');"
mapboxgl.accessToken = 'pk.eyJ1IjoiZmFiaWFuYW5jYXJkb3pvIiwiYSI6ImNreXd1MmV6MjBjNHoydnA4cW40cDR2eGMifQ.9jbCRmY4tRQe99Htcj5OIg';



//Esto es una validación, para si el usuario no tiene Geolocation, la aplicación no va a poder saber su ubicación
//Este navigation.geolocation lo puedo llamar en varios lugares, pero lo voy a llamar en PlacesProvider
if (!navigator.geolocation){
  alert('Tu navegador no tiene opción de Geolocation');
  throw new Error ('Tu navegador no tiene opción de Geolocation')
}

ReactDOM.render(
  <React.StrictMode>
    <MapsApp/>
  </React.StrictMode>,
  document.getElementById('root')
);

