//Crear el mapa
import { useContext, useLayoutEffect, useRef } from "react"
import {Map} from "mapbox-gl";

import { PlacesContext } from "../context/places/PlacesContext";
import { Loading } from "./Loading";
import { MapContext } from "../context/map/MapContext";

//aqui es donde renderizo el mapa de mapbox
export const MapView = () => {
    
    const { isLoading, userLocation } = useContext(PlacesContext);
    
    //mando el setMap al contexto
    const { setMap } = useContext (MapContext);




    //con esto me aseguro de poder ver varios mapas
    // useRef siempre regresa un objeto mutable con una única propiedad current
    const mapDiv = useRef<HTMLDivElement>(null);


    //para renderizar el map necesito que la constante mapDiv tenga un valor, para esto utilizo un useEffect
    //useLayoutEffect se ejecutará solamente y cada vez que el isLoading cambia
    useLayoutEffect(() => {
        if (!isLoading){
            const map = new Map({
                container: mapDiv.current!, // container ID, con ! digo que se que voy a tener un valor
                style: 'mapbox://styles/mapbox/streets-v11', // style URL
                center: userLocation, // aqui detectamos la ubicación central
                zoom: 14 // starting zoom
                });

                setMap( map );
        }
    }, [isLoading]);
    //Lo siguiente es colocar la variable map en lo más alto de la aplicación para que todos los componentes puedan utilizarlos
    




    //si esta cargando mostrara el loading, sino mostrara la geolocalización
    if (isLoading){
        return(<Loading/>)
    }
    
    return (
        <div ref={mapDiv}
            style={{
                height: '100vh',
                width: '100vw',
                position: 'fixed',
                top: 0,
                left: 0,
            }}
        >
            { userLocation?.join(',') }
        </div>
    )
}