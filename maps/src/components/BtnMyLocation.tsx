import { useContext } from "react"
import { MapContext } from "../context/map/MapContext";
import { PlacesContext } from "../context/places/PlacesContext";

export const BtnMyLocation = () => {


    //los dos contextos donde se encuentran las condiciones de la funcion onClick
    const {map, isMapReady} = useContext (MapContext);
    const {userLocation} = useContext (PlacesContext);

    //para que el button funcione creamos la constante onClick
    const onClick = () => {
        //para que funcione me voy a asegurar que dos condiciones se cumplan, estas dos condiciones se encuentran en contextos diferentes
        if(!isMapReady) throw new Error ('El mapa no esta listo');
        if(!userLocation) throw new Error ('No hay ubicación de usuario');
    
        //si la ubicacion existe
        map?.flyTo({
            zoom: 14,
            center: userLocation,
        })
    
    }




    //Cuando haga click en el button, llamaremos a la funcion onClick
    return (
        <button 
            className="btn btn-primary"
            onClick={onClick}
            style={{
                position: 'fixed',
                top: '20px',
                right: '20px',
                zIndex: 999,

                backgroundColor: '#00aae4',
                border: 'none',
                color: 'white',
                padding: '15px 32px',
                textAlign: 'center',
                textDecoration: 'none',
                display: 'inline-block',
                fontSize: '16px',
                borderRadius: '6px',
            }}
        >
            Mi Ubicación
        </button>
    )
}