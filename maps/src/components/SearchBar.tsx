import { ChangeEvent, useRef } from "react"

export const SearchBar = () => {

    //para que no se dupliquen las peticiones http. con debounceRef la accion se emite cuando el usuario no tipea nada por 500 milesimas de segundos
    const debounceRef = useRef<NodeJS.Timeout>()

    //Creo una funcion para que funcione el debounceRef
    const onQueryChanged = (event: ChangeEvent<HTMLInputElement>) => {
            
        //ejecutar esta condicion si tiene un valor
        if (debounceRef.current)
            clearTimeout(debounceRef.current);

            debounceRef.current = setTimeout(() => {
                //todo: buscar o ejecutar consulta
                console.log ('debounced value:', event.target.value);
            }, 350 );

    }


    //cada vez que la persona cambie durante el onChange, yo voy a tener el evento
    return (
        <div style={{
            position: 'fixed',
            top: '20px',
            left: '20px',
            backgroundColor: 'white',
            zIndex: 999,
            boxShadow: '0px 5px 10 px rgba(0,0,0,0.2)',
            width: '170px',
            padding: '5px',
            borderRadius: '5px',
        }}>

            <input
                type="text"
                className="form-control"
                placeholder="Buscar lugar..."
                onChange={onQueryChanged}
            />

        </div>
    )
}