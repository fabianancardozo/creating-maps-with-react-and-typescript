import axios from "axios";

const shearchApi = axios.create({
    baseURL:'https://api.mapbox.com/geocoding/v5/mapbox.places',
    params: {
        limit:5,
        language: 'it',
        access_token: 'pk.eyJ1IjoiZmFiaWFuYW5jYXJkb3pvIiwiYSI6ImNreXd1MmV6MjBjNHoydnA4cW40cDR2eGMifQ.9jbCRmY4tRQe99Htcj5OIg',
    }
})

export default shearchApi;