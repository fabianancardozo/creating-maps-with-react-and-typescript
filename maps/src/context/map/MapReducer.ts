import { Map } from "mapbox-gl"
import { MapState } from "./MapProvider"

type MapAction = {type: 'setMap', payload: Map}

export const MapReducer = (state: MapState, action: MapAction):MapState => {

    //Establecer el Mapa en Context
    switch (action.type) {
        case 'setMap': //voy a trabajar la accion cuando sea de tipo setMap
            return{
                ...state, //retorno una copia actual del estado
                isMapReady: true, //establexco el isMapReady en true
                map: action.payload //y estare esperando que el mapa es igual a action.payload (es decir el mapa)
            }
            
            //break;
    
        default:
            return state;
    }
}