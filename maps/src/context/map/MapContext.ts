//1 vcreamos una function MapContext y la interface MapContextProps
import { createContext } from "react";
import { Map } from "mapbox-gl";

export interface MapContextProps {
    isMapReady: boolean;
    map?: Map;

    //Metodos
    setMap: (map: Map) => void
}




export const MapContext = createContext ({} as MapContextProps)