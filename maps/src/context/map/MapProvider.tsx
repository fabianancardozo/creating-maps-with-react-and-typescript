import { useReducer } from "react";
import { Map, Marker, Popup } from "mapbox-gl"

import { MapContext } from "./MapContext"
import { MapReducer } from "./MapReducer";



export interface MapState {
    isMapReady: boolean;
    map?: Map;
}

const INITIAL_STATE: MapState = {
    isMapReady: false,
    map: undefined,
}

//para que pueda recibir childrens
interface Props {
    children: JSX.Element | JSX.Element [];
}

//esto es lo que otros componentes pueden observar, y la informacion que yo les comparto es la que esta en el value de return
//la información que compartire es el ...state y funciones.
export const MapProvider = ({children}: Props) => {

    const [state, dispatch] = useReducer ( MapReducer, INITIAL_STATE );

    const setMap = ( map: Map ) => {

        //info
        const myLocationPopup = new Popup()
        .setHTML(`
        <h4>Aquí estoy</h4>
        <p>En algún lugar del mundo</p>
        `)

        //creo el marcador del mapa, como no necesito almacenarlo, no es necesario que cree una constante
        new Marker ()
            //Esta es la ubicación central del mapa (latitud y longitud del usuario), lo que me da esta posicion es map.getCenter
            .setLngLat( map.getCenter() )
            //a donde quiero instalar o colocar este marcador
            .addTo( map );


        dispatch( { type: 'setMap', payload: map } )
    }


    return(
        <MapContext.Provider value={{
            ...state,
            
            setMap,
        }}>
            {children}
        </MapContext.Provider>
    )
}
