//3.- Manejar los estados lugares
//PlacesReducer retorna un objeto del mismo tipo del estado
//El reducer siempre tiene que devolver algo de PlacesState SIEMPRE. 
//PlacesState es un objeto que en este caso tiene el isLoading y useLocation
//El state sabemos que es de PlacesState, pero la action aun no esta definida
//defino la action como type ((uso type cuando se que no tengo necesidad de extenderlo))
//en payload voy a tener la latitud y longitud
//Trabajamos el Reducer con un SWITCH

//vuelvo a PlacesProvider

import { type } from "os";
import { PlacesState } from "./PlacesProvider";

type PlacesAction = {type: 'setUserLocation', payload: [number, number]};


export const PlacesReducer = (state: PlacesState, action: PlacesAction): PlacesState => {

    switch (action.type) {
        case 'setUserLocation':
            return{
                ...state, //hago una copia del state
                isLoading: false, //com ya termine de cargarlo o pongo en false
                userLocation: action.payload 
            }
    
        default:
            return state;
    }
}