//2 - Creo el context
//creo una funcion
//cre una interfaz para definir como luce la function PlacesContext 
//el {} as PlacesContextProps me sirve para no tener que volver a mencionar las propiedades de INITIAL_STATE dentro de la funcion
import { createContext } from "react";

export interface PlacesContextProps {
    isLoading: boolean;
    userLocation?: [number, number];
}

export const PlacesContext = createContext<PlacesContextProps>({} as PlacesContextProps);
