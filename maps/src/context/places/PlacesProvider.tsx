//1.- Defino el STATE: 
//creo una funcion pura
//creo una interfaz con dos propiedades para definir como quiero que luzca el objeto: placeState
//creo una interfaz INITIAL_STATE para definir el estado de placesState
//Importo placesContext en return 
//Desectruro children para poder importar placesProvide en MapsApp.tsx sin errores
//creo una constante useReducer, dentro de la funcion y le paso como parametros el componente placesReduces y la constante INITIAL_STATE 
//dentro de return, dentro del PlacesContext, expando el state de la constante [state, dispatch]

//lo que hicimos fue crear un espacio en la app para mantener el state y el state es una infomacion de como luce la app con INITIAL_STATE

//Con un useEffect Llamo al navigator.geolocation creado en index.tsx, este solo se va a ejecutar al inicio cuando PlacesProvider sea ejecutado

import { useEffect, useReducer } from "react";
import shearchApi from "../../apis/searchApi";
import { getUserLocation } from "../../herpers/getUserLocation";
import { PlacesContext } from "./PlacesContext"
import { PlacesReducer } from "./PlacesReducer";

export interface PlacesState {
    isLoading: boolean;
    userLocation?: [number, number];
}

const INITIAL_STATE: PlacesState = {
    isLoading: true,
    userLocation: undefined,
}

export interface Props {
    children: JSX.Element | JSX.Element []
}


export const PlacesProvider = ({children}: Props) => {

    const [state, dispatch] = useReducer (PlacesReducer, INITIAL_STATE);

    useEffect(() => {
        getUserLocation() //la geolocalización del usuario la podria crear aqui, pero me quedaria una funcion muy grande, para esto creo en helpers un getUserLocation.ts
        .then(lngLat => dispatch({ type:'setUserLocation', payload: lngLat }))
    }, []);

    const searchPlacesByTerm = async (query: string ) => {
        if (query.length === 0) return []; //Todo: limpiar state
        if (!state.userLocation) throw new Error ('No hay ubicación del usuario');

        const resp = await shearchApi.get(`/${query}.json`, {
            params:{
                proximity: state.userLocation.join(',')
            }
        })
    }
    



    return (
        <PlacesContext.Provider value={{
            ...state
            
        }}>
        {children}
        </PlacesContext.Provider>
    )
}
//Obtener la geolocalizacion del usuario