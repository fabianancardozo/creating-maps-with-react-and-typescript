//aqui colocamos todos los estados, todos los provider

import { PlacesProvider } from "./context/places/PlacesProvider";
import { HomeScreen } from "./screens/HomeScreen";

import './index.css';
import { MapProvider } from "./context/map/MapProvider";


export const MapsApp = () => {
    return (
        <PlacesProvider>
            <MapProvider>
                <HomeScreen/>
            </MapProvider>
            
        </PlacesProvider>
        
    )
}
//Crear contexto de lugares --- Places: se puede crear en 2 file pero aqui lo hago en 3
//Llamo PlacesProvider aca



//Creo un context de maps