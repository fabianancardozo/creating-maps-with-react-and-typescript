import { BtnMyLocation } from "../components/BtnMyLocation"
import { MapView } from "../components/MapView"
import { SearchBar } from "../components/SearchBar"

export const HomeScreen = () => {
    return (
        <div>
            <MapView/>
            <BtnMyLocation/>
            <SearchBar/>
        </div>
    )
}